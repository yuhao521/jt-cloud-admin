package com.cy.jt.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserDto implements Serializable {
    private static final long serialVersionUID = 129856911079058364L;
    private Integer id;
    private String username;
    private String password;
    private String nickname;
    private List<String> permissions;
}
