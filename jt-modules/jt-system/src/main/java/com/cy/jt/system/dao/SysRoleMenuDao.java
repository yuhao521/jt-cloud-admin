package com.cy.jt.system.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysRoleMenuDao {
    @Delete("delete from sys_role_menus where role_id=#{roleId}")
    int deleteByRoleId(Integer roleId);

    int insertRoleMenus(@Param("roleId") Integer roleId,@Param("menuIds") List<Integer> menuIds);

    List<Integer> selectMenuIdsByRoleId(@Param("roleIds") List<Integer> roleIds);
}
