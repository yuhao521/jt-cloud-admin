package com.cy.jt.service;

import com.cy.jt.system.domain.SysUser;
import com.cy.jt.system.service.SysUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;

@SpringBootTest
public class UserServiceTests {
    @Autowired
    private SysUserService sysUserService;
    @Test
    void testSelectUser(){
        SysUser sysUser=
        sysUserService.selectUserByUsername("tony");
        System.out.println(sysUser);
    }
}
